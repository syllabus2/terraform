## Introduction to Infrastructure as a code     1hr
-	**Overview of infrastructure as code (Iac)** 
## Overview of Terraform 
- **What is Terraform ?**
- **How does Terraform work?**
-  **Why Terraform?**
- **Features of Terraform**

  

## Setting Up Lab for Terraform   1hr
-	**Terraform installation on windows/Linux**
-	**Gitbash installation**
-	**Vscode installation and configuration**
-	**Install Terraform plugins in Vscode**
-	**Setting up google/Azure/Aws cloud account**

## Terraform use cases:
 - **Multi-Cloud Deployment**
-   **Application Infrastructure Deployment, Scaling, and Monitoring Tools**
-   **Self-Service Clusters**
-   **Policy Compliance and Management**
-   **PaaS Application Setup**
-   **Parallel Environments**
-   **Software Demos**



Source https://www.terraform.io/intro/use-cases
## Terraform configuration files and Terraform DSL    2 hrs
-	**Hashicorp configuration language (Hcl)** 
-	**Terraform configuration files(Desired Configuration)**
- **Configuration Syntax**
- **Arguments and Blocks**
- **Identifiers**
- **Comments**




 

## Know Terraform Providers      2 hrs
- **What Providers Do**
- **Where Providers Come From**
- **How to Use Providers**
- **Provider Installation**

**Few Providers** 
 -	Local
 -	Google Cloud
 -	Random
 -	AWS
 -	Azurerm



## Terraform resources  and Datasources     2hrs
- 	**Resource Syntax**
- **Resource Types**
- **Resource Arguments**
- **Using Data Sources**
- **Data Source Arguments**
- **Data Resource Behavior**




## Meta-Arguments
 - **depends_on**
-  **count**
-  **for_each**
-  **provider**
-  **lifecycle**
## Terraform Variables    2 hrs
-	**Variable Block**
-	**Approaches for Variable Assignment**
-	**Fetching data from Map and List**
-	**Local Values**
- **Declaring an Input Variable**
- **variable Arguments**
- **Default values**
- **Type Constraints**
- **Datatypes for variables**
- **Custom Validation Rules**
- **Suppressing Values in CLI Output**
- **Assigning Values to Root Module Variables**
- **Variables on the Command Line**
- **Variable Definitions (.tfvars) Files**
- **Environment Variables**
- **Variable Definition Precedence**

## TERRAFORM PROVISIONER   2 hrs
-	**Understanding Provisioners in Terraform**
-	**Types of Provisioners**
-	**Implementing remote-exec provisioners**
-	**Implementing local-exec provisioners**
-	**Creation-Time & Destroy-Time Provisioners**
-	**Failure Behavior for Provisioners**
## Terraform Modules    2 hrs
-	**Understand DRY principle**
-	**Module Blocks**
- **Calling a Child Module**
- **Meta Arguments for Terraform Module**
- **variables in modules**
- **output in Terraform modules**
- **Creating Local Modules**
- **Publishing modules to Github**
- **Publishing modules to Terraform Registry**
-	**Terraform Registry**
 
## STATE MANAGEMENT    2 hrs
- **Purpose**
- **Backends**
- **state storage and Locking**
- **Import Existing Resource**
-	**Integrating with GIT for team management**
-	**Security Challenges in Commiting TFState to GIT**
-	**Terraform .gitignore and .terraform**
-	**Remote State Management with Terraform**
-	**Terraform state pull**
-	**Terraform refresh on state**
## Terraform workspace
- **Purpose**
- **Using Workspaces**
- **Current Workspace Interpolation**
- **When to use Multiple Workspaces**
- **Workspace Internals**
## Terraform Expressions
- **Types and Values**
- **Strings and Templates**
- **References to Values**
- **Operators**
- **Function Calls**
- **Conditional Expressions**
- **For Expressions**
- **Splat Expressions**
- **Dynamic Blocks**
- **Custom Condition Checks**
- **Type Constraints**
- **Version Constraints**
## Terraform Functions
 - **Overview**
- **Numeric Functions**
- **String Functions**
- **Collection Functions**
- **Encoding Functions**
- **Filesystem Functions**
- **Date and Time Functions**
- **Hash and Crypto Functions**
- **IP Network Functions**
- **Type Conversion Functions**



## Terraform Settings

## Terraform Upgrade Guides
- **Overview**
- **Upgrading to Terraform v1.2**
- **Upgrading to Terraform v1.1**
- **Upgrading to Terraform v1.0**
- **v1.0 Compatibility Promises**
- **Upgrading to Terraform v0.15**
- **Upgrading to Terraform v0.14**
- **Upgrading to Terraform v0.13**
- **Upgrading to Terraform v0.12**
- **Upgrading to Terraform v0.11**
- **Upgrading to Terraform v0.10**
- **Upgrading to Terraform v0.9**
- **Upgrading to Terraform v0.8**
- **Upgrading to Terraform v0.7**

## TERRAFORM CLOUD & ENTERPRISE CAPABILITIES    2hrs
-	**Terraform cloud enterprise**
-	**Terraform cloud**
-	**Terraform cloud workflow**
-	**Terraform enterprise**
-	**Terraform registry**
-	**Terraform best practices** 

### Infrastructure as Code with Terraform and GitLab    2 hrs
-	**GitLab CI - Introduction**
-	**GitLab - CI/CD**
-	**GitLab - CI/CD Variables**
-	**GitLab CI - Permissions**
-	**Configuring GitLab Runners**
-	**Terraform and Gitlab Integration**

### Infrastructure as Code with Terraform and Jenkins    2 hrs
-	**Introduction to Jenkins**
-	**Jenkins - CI/CD Variables**
-	**Jenkins Plugins**
-	**Jenkins Pipelines**
-	**Jenkins Triggers**
-	**Jenkins Upstream and Downstream jobs**
-**Configuring Jenkins Agents**
-	**Execute Terraform with Jenkins Pipeline**
